package com.board.controller;

import java.io.IOException;

import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.BoardDAO;
import com.board.vo.BoardVO;
import com.google.gson.Gson;

@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		int    currentPage = Integer.parseInt(request.getParameter("currentPage"));
		String searchText = request.getParameter("searchText");
		String searchField = request.getParameter("searchField");
		BoardDAO dao = BoardDAO.getInstance();
		ArrayList<BoardVO> voList = null;
		try {
			voList = dao.list(currentPage, searchText, searchField);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String json = gson.toJson(voList);
		response.getWriter().write(json.toString());
	    
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
