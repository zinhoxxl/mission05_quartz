package com.board.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBConnection {

	public static Connection getConnection() throws Exception {
		Connection conn = null;
		try {
			Context    initContext = new InitialContext();
			Context    envContext  = (Context) initContext.lookup("java:/comp/env");
			DataSource ds          = (DataSource) envContext.lookup("jdbc/board");
			conn = ds.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	// 작업 수행 후 리소스 해제 - select (ResultSet)
	public static void selectClose(Connection conn, PreparedStatement pstmt, ResultSet rs) throws SQLException {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// 작업 수행 후 리소스 해제 - update, insert, delete
	public static void uidClose(Connection conn, PreparedStatement pstmt) throws SQLException {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
}
