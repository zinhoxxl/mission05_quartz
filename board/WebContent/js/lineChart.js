/**
 * lineChart.js
 */


window.onload = function() {
	lineChartDraw();
}


function lineChartDraw() {
	$.ajax({
		type: 'GET',
		url: '/board/LineChartServlet',
		data: 'json',
		success: function(data) {
			var json = JSON.parse(data);
			console.log(json);
			var labels = [dateFormat(35), dateFormat(28), dateFormat(21), dateFormat(14), dateFormat(7), dateFormat(0)];
			var lineChartData = {
				labels: labels,
				datasets: [{
					label: 'My First Dataset',
					data: [
						{x:1, y:json.sixWeeks},
						{x:2, y:json.fiveWeeks},
						{x:3, y:json.fourWeeks},
						{x:4, y:json.threeWeeks},
						{x:5, y:json.twoWeeks},
						{x:6, y:json.oneWeeks}
					],
					fill: false,
					borderColor: 'rgb(75, 192, 192)',
					lineTension: 0.1
				}]
			}
			var ctx = document.getElementById('line-chart').getContext("2d");
			window.lineChart = new Chart(ctx, {
				type: 'line',
				data: lineChartData,
				options: {
					responsive: true,
					maintainAspectRatio: false,
					title: {
						display: true,
						text: '최근 6주 간 게시물 등록 추이'
					}
				}
			});
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
}


function dateFormat(dayAfter) {	
	var today = new Date();
	var monthDay = today.getDate();
	today.setDate(monthDay - dayAfter);
    var month = today.getMonth() + 1;
    var day = today.getDate();   
    month = month >= 10 ? month : '0' + month;
    day = day >= 10 ? day : '0' + day;

    return today.getFullYear() + '-' + month + '-' + day;
}
