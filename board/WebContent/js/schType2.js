/**
 * 스케줄러 type2
 */



/* 재기동시 이전 설정정보에 따라 동작 */
$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/board/Sch02StateServlet",
		success: function(data) {
			var result = document.getElementById('scheduler02');
			if (data == 'Running') {
				result.value = '스케쥴러 #2 : Running';
				sch02Run();
			} else {
				result.value = '스케쥴러 #2 : Stopped';
			}
		},
		error: function(request, error) {
			alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
		}
	});
});



/* 버튼 클릭시 RUN */
$(function() {
	$('#sch2Run').click(function() {
		sch02Run();
	});
});



/* 버튼 클릭시 STOP */
$(function() {
	$('#sch2Stop').click(function() {
		$.ajax({
			type: 'GET',
			url: '/board/Sch02StopServlet',
			success: function() {
				alert('스케줄러#2 중지');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
});



/* RUN function */
function sch02Run() {
	var term2 = $('#term2 option:selected').val();
	$.ajax({
		type: 'GET',
		url: '/board/Sch02RunServlet',
		data: { "term2": term2 },
		success: function() {
			alert('스케줄러#2 실행');
		},
		error: function(request, error) {
			alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
		}
	});
};
