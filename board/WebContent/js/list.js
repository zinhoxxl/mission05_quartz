/**
 * list.js
 */


$(document).ready(function() {
	list(1);
});



function search() {
	list(1);
}



function list(pageNum) {
	var text = $("#searchText").val();
	var searchField = $("#searchField").val();
	if (pageNum == null) {
		pageNum = 1;
	}
	var parameter = {
		currentPage: pageNum,
		searchText: text,
		searchField: searchField
	};
	$.ajax({
		type: 'GET',
		url: '/board/ListServlet',
		data: parameter,
		success: function(data) {
			var str = '';
			var json = JSON.parse(data);
			$.each(json, function(i) {
				str += '<tr>';
				str += '<th>' + json[i].board_num + '</th><th>' + json[i].title + '</th><th><a href="#" onClick="loadPopup(' + json[i].board_num + ')">' + json[i].content + '</a></th><th>' + json[i].writeDate + '</th>';
				str += '</tr>';
			});
			$('#boardList').html(str);
			pageInfo(pageNum);
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
}



function pageInfo(pageNum) {
	var text = $("#searchText").val();
	var searchField = $("#searchField").val();

	var parameter = {
		currentPage: pageNum,
		searchText: text,
		searchField: searchField
	};
	$.ajax({
		type: 'GET',
		url: '/board/PageServlet',
		data: parameter,
		success: function(data) {

			var view = '<ul class = "pagination justify-content-center">';
			var json = JSON.parse(data);

			// 이전 페이지
			if (json.pagePrev == true) {
				view += '<li class="page-item"><a class="page-link" href="javascript:list(' + (json.currentPage - 1) + ');"> Previous </a></li>';
			} else {
				view += '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true"> Previous </a></li>';
			}
			// 게시글 10행
			for (var nowPage = json.startPage - 1; json.endPage > nowPage; nowPage++) {
				if (pageNum == nowPage + 1) {
					view += '<li class="page-item disabled"><a class="page-link">' + (nowPage + 1) + '</a></li>';
				} else {
					view += '<li class="page-item" data-page="' + (nowPage + 1) + '" onclick="javascript:list(' + (nowPage + 1) + ');"><a class="page-item">' + (nowPage + 1) + '</a></li>';
				}
			}
			// 다음 페이지
			if (json.pageNext == true) {
				view += '<li class="page-item" onclick="javascript:list(' + (json.currentPage + 1) + ');"><a class="page-link"> Next </a></li>';
			} else {
				view += '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true"> Next </a></li>';
			}
			view += '</ul>';

			$("#pageArea").html(view);
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});

}




function loadPopup(data, w, h) {
	var url = './load.html?board_num=' + data;
	var screenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var screenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var left = ((width / 2) - (w / 2)) + screenLeft;
	var top = ((height / 2) - (h / 2)) + screenTop;

	window.open(url, data, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}



