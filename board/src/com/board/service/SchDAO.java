package com.board.service;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.board.db.DBConnection;
import com.board.vo.Sch_defVO;

public class SchDAO {

	private SchDAO() {}
	private static SchDAO instance;
	public static SchDAO getInstance() {
		if (instance == null)
			instance = new SchDAO();
		return instance;
	}

	
	/*
	 * Sch_def테이블 Sch#1 Running
	 */
	public void sch01RunningStatus(Sch_defVO vo) throws Exception {

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String id = "Sch#1";
		String Start_time = dateFormat.format(date);
		String status = "Running";

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "MERGE INTO sch_def USING dual ON (id = 'Sch#1') ";
		SQL += "WHEN NOT MATCHED THEN ";
		SQL += "INSERT (id, Start_time, Term, Status) ";
		SQL += "VALUES (?, ?, ?, ?) ";
		SQL += "WHEN MATCHED THEN ";
		SQL += "UPDATE SET ";
		SQL += "Start_time = ?, Term = ?, Status = ? ";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, id);
			pstmt.setString(2, Start_time);
			pstmt.setString(3, vo.getTerm1());
			pstmt.setString(4, status);
			pstmt.setString(5, Start_time);
			pstmt.setString(6, vo.getTerm1());
			pstmt.setString(7, status);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
	}

	
	/*
	 * Sch_def테이블 Sch#1 Stopped 
	 */
	public void sch01StoppedStatus() throws Exception {

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String id = "Sch#1";
		String end_time = dateFormat.format(date);
		String status = "Stopped";

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "MERGE INTO sch_def USING dual ON (id = 'Sch#1') ";
		SQL += "WHEN NOT MATCHED THEN ";
		SQL += "INSERT (id, End_time, Status) ";
		SQL += "VALUES (?, ?, ?) ";
		SQL += "WHEN MATCHED THEN ";
		SQL += "UPDATE SET ";
		SQL += "End_time = ?, Status = ? ";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, id);
			pstmt.setString(2, end_time);
			pstmt.setString(3, status);
			pstmt.setString(4, end_time);
			pstmt.setString(5, status);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
	}

	
	/*
	 * Sch_def테이블 Sch#2 Running
	 */
	public void sch02RunningStatus(Sch_defVO vo) throws Exception {

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String id = "Sch#2";
		String Start_time = dateFormat.format(date);
		String status = "Running";

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "MERGE INTO sch_def USING dual ON (id = 'Sch#2') ";
		SQL += "WHEN NOT MATCHED THEN ";
		SQL += "INSERT (id, Start_time, Term, Status) ";
		SQL += "VALUES (?, ?, ?, ?) ";
		SQL += "WHEN MATCHED THEN ";
		SQL += "UPDATE SET ";
		SQL += "Start_time = ?, Term = ?, Status = ? ";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, id);
			pstmt.setString(2, Start_time);
			pstmt.setString(3, vo.getTerm2());
			pstmt.setString(4, status);
			pstmt.setString(5, Start_time);
			pstmt.setString(6, vo.getTerm2());
			pstmt.setString(7, status);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
	}

	
	/*
	 * Sch_def테이블 Sch#2 Stopped
	 */
	public void sch02StoppedStatus() throws Exception {

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String id = "Sch#2";
		String end_time = dateFormat.format(date);
		String status = "Stopped";

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "MERGE INTO sch_def USING dual ON (id = 'Sch#2') ";
		SQL += "WHEN NOT MATCHED THEN ";
		SQL += "INSERT (id, End_time, Status) ";
		SQL += "VALUES (?, ?, ?) ";
		SQL += "WHEN MATCHED THEN ";
		SQL += "UPDATE SET ";
		SQL += "End_time = ?, Status = ? ";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, id);
			pstmt.setString(2, end_time);
			pstmt.setString(3, status);
			pstmt.setString(4, end_time);
			pstmt.setString(5, status);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
	}

	/*
	 * 스케줄러1번 Running 인지 Stopped 인지 확인
	 * */
	public Sch_defVO sch01StatusCheck(Sch_defVO vo) throws Exception {
		String id = "Sch#1";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT status FROM sch_def WHERE id = ?";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				vo = new Sch_defVO();
				vo.setStatus1(rs.getString("status"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.selectClose(conn, pstmt, rs);
			} catch (Exception e) {
				throw e;
			}
		}
		return vo;
	}

	
	/*
	 * 스케줄러2번 Running 인지 Stopped 인지 확인
	 * */
	public Sch_defVO sch02StatusCheck(Sch_defVO vo) throws Exception {
		String id = "Sch#2";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT status FROM sch_def WHERE id = ?";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				vo = new Sch_defVO();
				vo.setStatus2(rs.getString("status"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.selectClose(conn, pstmt, rs);
			} catch (Exception e) {
				throw e;
			}
		}
		return vo;
	}

}
