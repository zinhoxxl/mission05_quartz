package com.board.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.SchDAO;
import com.board.vo.Sch_defVO;

@WebServlet("/Sch02StateServlet")
public class Sch02StateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Sch02StateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		try {
			Sch_defVO vo = new Sch_defVO();
			SchDAO dao = SchDAO.getInstance();
			try {
				vo = dao.sch02StatusCheck(vo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String statusCheck2 = vo.getStatus2();
    		response.getWriter().write(statusCheck2);
    	} catch (Exception e) { 
    		e.printStackTrace(); 
    	}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
