package com.board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.BoardDAO;
import com.board.vo.BoardVO;
import com.google.gson.Gson;

@WebServlet("/LoadServlet")
public class LoadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoadServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int board_num = Integer.parseInt(request.getParameter("board_num"));

		BoardDAO dao = BoardDAO.getInstance();
		BoardVO vo = null;
		try {
			vo = dao.load(board_num);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		String json = gson.toJson(vo);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
