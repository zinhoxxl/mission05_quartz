package com.board.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.board.service.Sch02Job;
import com.board.service.SchDAO;
import com.board.vo.Sch_defVO;


@WebServlet("/Sch02RunServlet")
public class Sch02RunServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private SchedulerFactory schedulerFactory;
	private Scheduler scheduler;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String term2 = request.getParameter("term2");
		Sch_defVO vo = new Sch_defVO();
		vo.setTerm2(term2);
		
		SchDAO dao = SchDAO.getInstance();
		try {
			dao.sch02RunningStatus(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			schedulerFactory = new StdSchedulerFactory();
			scheduler = schedulerFactory.getScheduler();
			scheduler.start();

			JobDetail job = JobBuilder.newJob(Sch02Job.class)
					.withIdentity("job4", "group4")
					.build();
			switch (term2) {
				case "10段" : 
					Trigger trigger1 = (Trigger) TriggerBuilder.newTrigger() 
							.withIdentity("trigger5", "group5") 
							.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
									.withIntervalInSeconds(10)
									.repeatForever()) 
							.startAt(new Date(System.currentTimeMillis() + 10000)) 
							.build();
					scheduler.scheduleJob(job, trigger1);
					break;
				case "20段" : 
					Trigger trigger2 = (Trigger) TriggerBuilder.newTrigger() 
							.withIdentity("trigger5", "group5") 
							.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
									.withIntervalInSeconds(20)
									.repeatForever()) 
							.startAt(new Date(System.currentTimeMillis() + 20000)) 
							.build();
					scheduler.scheduleJob(job, trigger2);
					break;
				case "30段" : 
					Trigger trigger3 = (Trigger) TriggerBuilder.newTrigger() 
							.withIdentity("trigger5", "group5") 
							.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
									.withIntervalInSeconds(30)
									.repeatForever()) 
							.startAt(new Date(System.currentTimeMillis() + 30000)) 
							.build();
					scheduler.scheduleJob(job, trigger3);
					break;
				case "40段" : 
					Trigger trigger4 = (Trigger) TriggerBuilder.newTrigger() 
							.withIdentity("trigger5", "group5") 
							.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
									.withIntervalInSeconds(40)
									.repeatForever()) 
							.startAt(new Date(System.currentTimeMillis() + 40000)) 
							.build();
					scheduler.scheduleJob(job, trigger4);
					break;
				case "50段" : 
					Trigger trigger5 = (Trigger) TriggerBuilder.newTrigger() 
							.withIdentity("trigger5", "group5") 
							.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
									.withIntervalInSeconds(50)
									.repeatForever()) 
							.startAt(new Date(System.currentTimeMillis() + 50000)) 
							.build();
					scheduler.scheduleJob(job, trigger5);
					break;
				case "60段" : 
					Trigger trigger6 = (Trigger) TriggerBuilder.newTrigger() 
							.withIdentity("trigger5", "group5") 
							.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
									.withIntervalInSeconds(60)
									.repeatForever()) 
							.startAt(new Date(System.currentTimeMillis() + 60000)) 
							.build();
					scheduler.scheduleJob(job, trigger6);
					break;
			}
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
