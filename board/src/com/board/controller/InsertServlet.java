package com.board.controller;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.BoardDAO;
import com.board.vo.BoardVO;
import com.google.gson.Gson;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

@WebServlet("/InsertServlet")
public class InsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public InsertServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String fileName = request.getParameter("fileName");
		String filePath = request.getParameter("filePath");

		try {
			filePath = "C:\\fileUpload";
			int maxSize = 10 * 1024 * 1024;
			String encoding = "UTF-8";

			MultipartRequest multi = new MultipartRequest(request, filePath, maxSize, encoding, new DefaultFileRenamePolicy());
			title = multi.getParameter("title");
			content = multi.getParameter("content");
			fileName = multi.getOriginalFileName("fileName");
		} catch (Exception e) {
			e.printStackTrace();
		}

		BoardVO vo = new BoardVO();
		vo.setTitle(title);
		vo.setContent(content);
		vo.setFileName(fileName);
		vo.setFilePath(filePath);

		BoardDAO dao = BoardDAO.getInstance();
		try {
			dao.insert(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String json = new Gson().toJson("json");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json);
	}

}
