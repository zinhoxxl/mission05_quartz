-- 게시판 테이블 생성
CREATE TABLE board (
    board_num NUMBER(4) PRIMARY KEY,
    title VARCHAR2(100),
    content VARCHAR2(1000)
);
alter table board add fileName varchar2(1000);
alter table board add filePath varchar2(2000);
COMMENT ON COLUMN board.board_num IS 'Index';
COMMENT ON COLUMN board.title IS '제목';
COMMENT ON COLUMN board.content IS '내용';

-- 게시판 시퀀스 생성
CREATE SEQUENCE board_seq
       INCREMENT BY 1
       START WITH 1
       MINVALUE 1
       MAXVALUE 9999
       NOCYCLE
       NOCACHE
       NOORDER;
       
-- 게시글 
INSERT INTO board VALUES(board_seq.nextval, '첫번째 글', '플로우컨트롤 최고 만만세');
INSERT INTO board VALUES(board_seq.nextval, '두번째 글', '자나깨나 코로나 조심 자가격리는 심심해');
INSERT INTO board VALUES(board_seq.nextval, '세번째 글', '점심은 오리양념고기였다 맛있었다');

commit;
SELECT * from board;


-- 미션-2 게시판 검색, 첨부파일, 페이징 
create index IND_SEARCH on board(title, content);

select * from user_ind_columns;


select title from board;

SELECT * FROM USER_IND_COLUMNS WHERE TABLE_NAME='board';

commit;

select sysdate from dual;
ALTER TABLE board ADD writeDate DATE DEFAULT SYSDATE;
select to_char(writeDate, 'yy-mm-dd') from board;

select fileName from board where writeDate >= to_char(add_months(sysdate,-1), 'yyyy-mm-dd');
select count(fileName) as attachments from board;



select weeks, count(weeks) as count
from (
 SELECT CASE WHEN writeDate >= SYSDATE-7 THEN 'oneWeeks'
  when writeDate between SYSDATE-14 and SYSDATE-7 then 'twoWeeks' 
  when writeDate between SYSDATE-21 and SYSDATE-14 then 'threeWeeks'
  when writeDate between SYSDATE-28 and SYSDATE-21 then 'fourWeeks'
  when writeDate between SYSDATE-35 and SYSDATE-28 then 'fiveWeeks'
  when writeDate between SYSDATE-42 and SYSDATE-35 then 'sixWeeks'
  ELSE '0' END as weeks 
from board ) 
GROUP BY weeks order by decode(weeks, 'oneWeeks', 1, 'twoWeeks', 2, 'threeWeeks', 3, 'fourWeeks', 4, 'fiveWeeks', 5,'sixWeeks', 6) desc;


--------------------------------------------------
-- 스케줄러 테이블

CREATE TABLE sch_def (
    id VARCHAR2(200) PRIMARY KEY,
    Start_time VARCHAR2(500),
    End_time VARCHAR2(500),
    Term VARCHAR2(100),
    Status VARCHAR2(200)
);
alter session set nls_date_format='yyyy-MM-dd HH:mi:ss';

select * from board order by board_num desc;
select * from sch_def;



commit;