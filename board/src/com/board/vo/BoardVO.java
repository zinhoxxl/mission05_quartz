package com.board.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BoardVO {

	private int board_num;
	private String title;
	private String content;
	private String fileName;
	private String filePath;
	private int fileSize;
	private String writeDate;
	
	
	private int oneWeeks;
	private int twoWeeks;
	private int threeWeeks;
	private int fourWeeks;
	private int fiveWeeks;
	private int sixWeeks;
	

	public int getBoard_num() {
		return board_num;
	}

	public void setBoard_num(int board_num) {
		this.board_num = board_num;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(String writeDate) throws ParseException {
		SimpleDateFormat newDtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date formatDate = newDtFormat.parse(writeDate);
		String strNewDtFormat = newDtFormat.format(formatDate);
		this.writeDate = strNewDtFormat;
	}

	public int getOneWeeks() {
		return oneWeeks;
	}

	public void setOneWeeks(int oneWeeks) {
		this.oneWeeks = oneWeeks;
	}

	public int getTwoWeeks() {
		return twoWeeks;
	}

	public void setTwoWeeks(int twoWeeks) {
		this.twoWeeks = twoWeeks;
	}

	public int getThreeWeeks() {
		return threeWeeks;
	}

	public void setThreeWeeks(int threeWeeks) {
		this.threeWeeks = threeWeeks;
	}

	public int getFourWeeks() {
		return fourWeeks;
	}

	public void setFourWeeks(int fourWeeks) {
		this.fourWeeks = fourWeeks;
	}

	public int getFiveWeeks() {
		return fiveWeeks;
	}

	public void setFiveWeeks(int fiveWeeks) {
		this.fiveWeeks = fiveWeeks;
	}

	public int getSixWeeks() {
		return sixWeeks;
	}

	public void setSixWeeks(int sixWeeks) {
		this.sixWeeks = sixWeeks;
	}


	
	
	
}
