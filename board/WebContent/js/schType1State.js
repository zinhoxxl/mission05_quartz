/**
 * 스케줄러 type1 상태표시
 */


$(function() {
	$("#sch1Run").click(function() {
		$.ajax({
			type: "GET",
			url: "/board/Sch01StateServlet",
			success: function(data) {
				var result = document.getElementById('scheduler01');
				if (data) {
					result.value = '스케쥴러 #1 : Running';
				} else {
					result.value = '스케쥴러 #1 : Stopped';
				}
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
	$("#sch1Stop").click(function() {
			document.getElementById("scheduler01").value = '스케쥴러 #1 : Stopped';
	});
});


