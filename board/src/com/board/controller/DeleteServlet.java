package com.board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.BoardDAO;
import com.google.gson.Gson;

@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DeleteServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		int board_num = Integer.parseInt(request.getParameter("board_num"));

		BoardDAO dao = BoardDAO.getInstance();
		try {
			dao.delete(board_num);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String json = new Gson().toJson("json");
		response.setContentType("application/json");
		response.getWriter().write(json);
	}

}
