/**
 * insert.js
 */


function insertPopup(url, title, w, h) {

	var screenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var screenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var left = ((width / 2) - (w / 2)) + screenLeft;
	var top = ((height / 2) - (h / 2)) + screenTop;

	window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}


function insert() {
	var formBoard = $('#formBoard')[0];
	var formData = new FormData(formBoard);
	$.ajax({
		type: 'POST',
		url: '/board/InsertServlet',
		data: formData,
		processData: false,
        contentType: false,
		dataType: 'json',
		success: function() {
			alert("게시글 작성 완료");
			window.close();
			opener.location.href = './board.html';
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
}
