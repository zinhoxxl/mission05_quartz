package com.board.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DownloadServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/download;charset=UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		String realPath = "C:\\fileUpload";

		String fileName = request.getParameter("fileName");

		String UTF8FileNameAndPath = new String(fileName.getBytes("8859_1"), "UTF-8");

		String UTF8FileName = UTF8FileNameAndPath.substring(UTF8FileNameAndPath.lastIndexOf("/") + 1)
				.substring(UTF8FileNameAndPath.lastIndexOf(File.separator) + 1);

		boolean MSIE = request.getHeader("user-agent").indexOf("MSIE") != -1;

		String fileNameToSave = fileName;

		if (MSIE) {
			fileNameToSave = URLEncoder.encode(UTF8FileName, "UTF8").replaceAll("\\+", " ");
		} else {
			fileNameToSave = new String(fileName.getBytes("EUC-KR"),"8859_1");
		}

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileNameToSave, "UTF-8") + ";");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");

		String filePathAndName = realPath + UTF8FileNameAndPath;
		File file = new File(filePathAndName);

		byte bytestream[] = new byte[2048000];

		if (file.isFile() && file.length() > 0) {
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
			int read = 0;
			while ((read = bis.read(bytestream)) != -1) {
				bos.write(bytestream, 0, read);
			}
			try {
				bos.close();
				bis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
