/**
 * pieChart.js
 */


window.onload = function() {
	pieChartDraw();
}


function pieChartDraw() {
	$.ajax({
		type: 'GET',
		url: '/board/PieChartServlet',
		data: 'json',
		success: function(data) {
			var json = JSON.parse(data);
			var pieChartData = {
				labels: ['첨부파일 없는 게시물', '파일첨부 게시물'],
				datasets: [{
					data: [json.noneAttachments, json.attachments],
					backgroundColor: ['rgb(255, 99, 132)', 'rgb(255, 205, 86)']
				}]
			}
			var ctx = document.getElementById('pie-chart').getContext('2d');
			window.pieChart = new Chart(ctx, {
				type: 'pie',
				data: pieChartData,
				options: {
					responsive: true,
					maintainAspectRatio: false,
					title: {
						display: true,
						text: '한달 이내 첨부파일 게시물 차트 (총 건수: ' + json.attachmentsTotal + ')'
					}
				}
			});
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
}
