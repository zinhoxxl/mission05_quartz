/**
 * 스케줄러 type1
 */



/* 재기동시 이전 설정정보에 따라 동작 */
$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/board/Sch01StateServlet",
		success: function(data) {
			var result = document.getElementById('scheduler01');
				if (data == 'Running') {
					result.value = '스케쥴러 #1 : Running';
			        sch01Run();
				} else {
					result.value = '스케쥴러 #1 : Stopped';
				}
		},
		error: function(request, error) {
			alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
		}
	});
});




/* 버튼 클릭시 RUN */
$(function() {
	$('#sch1Run').click(function() {
		sch01Run();
	});
});



/* 버튼 클릭시 STOP */
$(function() {
	$('#sch1Stop').click(function() {
		$.ajax({
			type: 'GET',
			url: '/board/Sch01StopServlet',
			success: function() {
				alert('스케줄러#1 중지');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
});



/* RUN function */
function sch01Run(){
	var term1 = $('#term1 option:selected').val();
		$.ajax({
			type: 'GET',
			url: '/board/Sch01RunServlet',
			data: {"term1" : term1},
			success: function() {
				alert('스케줄러#1 실행');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
};
