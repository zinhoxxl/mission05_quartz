/**
 * delete.js
 */

$(document).on('click', '#delete', function() {

	var board_num = $('#board_num').val();

	var loadBoardNum = new FormData();
	loadBoardNum.append("board_num", board_num);

	$.ajax({
		type: 'POST',
		url: '/board/DeleteServlet',
		data: { "board_num": board_num },
		dataType: 'json',
		success: function() {
			if (confirm("게시글을 삭제하시겠습니까?")) {
				window.close();
				opener.location.href = './board.html';
			}
			else {
				return false;
			}
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
});
